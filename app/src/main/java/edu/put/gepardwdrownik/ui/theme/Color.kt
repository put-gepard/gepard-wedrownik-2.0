package edu.put.gepardwdrownik.ui.theme

import androidx.compose.ui.graphics.Color

val gepardColor = Color(0xFFAA6F17)
val gepardColorLight = Color(0xFFFFBC59)