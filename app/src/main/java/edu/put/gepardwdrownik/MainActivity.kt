package edu.put.gepardwdrownik

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.animation.doOnEnd
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import edu.put.gepardwdrownik.ui.theme.gepardColorLight
import kotlinx.coroutines.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import edu.put.gepardwdrownik.ui.theme.GepardWędrownikTheme

class Trail(
    val name: String,
    val description: String,
    val stopwatch: Stopwatch,
    val imageID: Int
) {
}


data class MenuItem(val id: String, val title: String, val icon: String)

class Stopwatch {
    var timeF by mutableStateOf("00:00")

    private var cscope = CoroutineScope(Dispatchers.Default)
    private var active = false
    private var timeMSec = 0L
    private var prevTime = 0L

    fun start() {
        if (active) return
        cscope.launch {
            prevTime = System.currentTimeMillis()
            active = true
            while (active) {
                delay(1000L)
                timeMSec += System.currentTimeMillis() - prevTime
                prevTime = System.currentTimeMillis()
                timeF = formatTime(timeMSec)
            }
        }
    }

    fun pause() {
        active = false
    }

    fun reset() {
        cscope.cancel()
        cscope = CoroutineScope(Dispatchers.Default)
        active = false
        timeMSec = 0L
        prevTime = 0L
        timeF = "00:00"
    }

    private fun formatTime(time: Long): String {
        val lDateTime = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(time),
            ZoneId.systemDefault()
        )

        val formatter = DateTimeFormatter.ofPattern("mm:ss", Locale.getDefault())
        return lDateTime.format(formatter)
    }
}

val trails = listOf(
    Trail("Seceda", "SECEDA to jedno z najbardziej spektakularnych miejsc w Dolomitach. Znajduje się u podnóża grupy szczytów Odle w rejonie Val Gardena. Tak w zasadzie Seceda to szczyt wznoszący się na wysokość 2.519 m n.p.m., z którego wspaniałe panoramiczne widoki na strzeliste szczyty zapierają dech w piersi.", Stopwatch(), R.drawable.seceda),
    Trail("Sandomierz – spacer optymistyczny", "Jeśli czujesz, że tracisz radość życia w codziennej gonitwie oraz brakuje Ci okazji do zamyślenia, chwili przyjemności, wspólnego spaceru… odnajdź swój przepis na szczęście!", Stopwatch(), R.drawable.sandomierz),
    Trail("Pomorska droga św. Jakuba", "Pomorska Droga św. Jakuba lub Camino Polaco del Norte (Droga Północnopolska) część Via Baltica (Droga Bałtycka). W średniowieczu trasa ta wiodła od Estonii przez Litwę, Polskę, Niemcy. Odtworzona w latach 2011-2013 przy wsparciu środków z Unii Europejskiej programu Europejskiej Współpracy Terytorialnej Południowy Bałtyk.", Stopwatch(), R.drawable.jakub),
    Trail("Szlak Solny", "Jest to szlak turystyczny powstały na bazie średniowiecznego traktu handlowego biegnącego z nad morza na południe kraju. Średniowieczni kupcy podróżowali wozami wypełnionymi drogocennym towarem - solą zakupioną w Kołobrzegu - do centralnych regionów kraju. Szlak oznaczono kolorem czerwonym, jego całkowita długość to 152 km, kołobrzeski odcinek to zaledwie 4km.", Stopwatch(), R.drawable.solny),
    Trail("Szlak im. Jana Frankowskiego", "Jeden z najkrótszych szlaków turystycznych województwa zachodniopomorskiego można odnaleźć w Kołobrzegu. Szlak znakowany jest kolorem zielonym i nosi imię wielkiego społecznika, miłośnika Kołobrzegu - Jana Frankowskiego, który zasłużył się jako twórca pierwszego powojennego przewodnika po mieście i okolicach. Pasjonat książek, mistrz bibliotekarstwa.", Stopwatch(), R.drawable.frankowski),
    Trail("Szlak Wyspy Chrząszczewskiej", "Szlak ma charakter liniowy zataczając przy tym pętlę na Wyspie Chrząszczewskiej. Prowadzi z Kamienia Pomorskiego PKS/PKP przez Królewski Głaz do Chrząszczewa. Na zachód od głazu Królewskiego występuje chaszczowanie czyli poruszanie się po nieistniejącej ścieżce wśród bujnej roślinności (oczywiście w zależności od pory roku).", Stopwatch(), R.drawable.wyspa),
    Trail("Szlak Brdy", "Wytyczony przede wszystkim z myślą o turystyce pieszej Szlak Bursztynowy w okolicach Gdańska liczy nieco ponad 34 km. Oznakowana kolorem żółtym trasa rozpoczyna się w Pruszczu Gdańskim przy stacji kolejowej, a kończy w Otominie, na rogatkach Gdańska. Szlak w dużej mierze wiedzie nad rzeką Radunią, która odkrywa przed turystą różne swe oblicza.", Stopwatch(), R.drawable.brda),
    Trail("Szlak Bursztynowy", "Szlak długi i trudny. Dużo odcinków trudnych technicznie - chaszcze bez ścieżek z szukaniem kierunku dalszego marszu zwłaszcza w okolicy linii brzegowej. Momentami zanikające (bardzo dawno nie odnawiane) oznakowanie na przemian z oznakowaniem nowym.", Stopwatch(), R.drawable.bursztyn),
)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val current = isSystemInDarkTheme()
            val colorScheme = remember { mutableStateOf(current) }
            GepardWędrownikTheme (
                darkTheme = colorScheme.value
            ){
                Navigation(colorScheme)
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun Navigation(colorScheme: MutableState<Boolean>) {
    val navigationController = rememberNavController()

    var isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE
    var landscapeOrientation by remember { mutableStateOf(isLandscape) }
    var currentTrail = remember { mutableStateOf<Trail?>(trails[0]) }

    NavHost(navController = navigationController, startDestination = Screen.MainScreen.route) {
        composable(route = Screen.MainScreen.route) {
            val scaffoldState = rememberScaffoldState()
            val s = rememberCoroutineScope()
            Scaffold(
                scaffoldState = scaffoldState,
                topBar = {
                    AppBar(onNavigationItemClick = {
                        s.launch {
                            scaffoldState.drawerState.open()
                        }
                    })
                },
                drawerContent = {
                    DrawerHeader()
                    DrawerBody(items = listOf(
                        MenuItem(
                            id = "1",
                            title = " Zmień motyw",
                            icon = "🎨"
                        ),
                    ), onItemClick = {
                        colorScheme.value = !colorScheme.value
                    })
                }, drawerGesturesEnabled = scaffoldState.drawerState.isOpen,
                content = {
                    if (landscapeOrientation) {
                        Row {
                            Box(
                                modifier = Modifier.weight(1f)
                            ) {
                                MainScreen(navigationController, currentTrail, true)
                            }
                            currentTrail?.value.let {
                                Box(
                                    modifier = Modifier.weight(1f)
                                ) {
                                    if (it != null) {
                                        Details(it.name, it.description, it.imageID, it.stopwatch)
                                    }
                                }
                            }
                        }
                    } else {
                        MainScreen(navigationController, currentTrail, false)
                    }
                })
        }
        composable(
            route = Screen.DetailScreen.route + "/{title}",
            arguments = listOf(
                navArgument("title") { type = NavType.StringType; nullable = false },
            )
        ) { entry ->
            currentTrail.value = trails.find { it.name == entry.arguments?.getString("title") }
            var currentTrailIdx = trails.indexOf(currentTrail.value)
            val pagerState = rememberPagerState(initialPage = currentTrailIdx, pageCount = { trails.size })
            LaunchedEffect(pagerState) {
                snapshotFlow { pagerState.currentPage }.collect() { page ->
                    Log.d("page", "page changed to $page")
                    currentTrail.value = trails[page]
                }
            }
            HorizontalPager(
                state = pagerState,
            ) { index ->
                val trail = trails[index]
                DetailScreen(
                    trail!!.name,
                    trail!!.description,
                    trail!!.imageID,
                    trail!!.stopwatch,
                    colorScheme
                )
            }
            if (landscapeOrientation && navigationController.previousBackStackEntry?.destination?.route == Screen.MainScreen.route) {
                navigationController.popBackStack()
                Log.d("composable", "popBackStack")
            }
        }
    }
}

@Composable
fun MainScreen(
    navController: NavController,
    currentTrail: MutableState<Trail?>,
    landscape: Boolean
) {
    LazyVerticalGrid(
        columns = GridCells.Adaptive(150.dp),
        modifier = Modifier.fillMaxWidth(),
        content = {
            items(trails) { trail ->
                Card(
                    modifier = Modifier
                        .background(gepardColorLight)
                        .clickable {
                            currentTrail.value = trail
                            if (!landscape) {
                                navController.navigate(Screen.DetailScreen.route + "/${trail.name}")
                            }
                        }
                        .fillMaxWidth()
                        .aspectRatio(1f)
                        .padding(2.dp),
                ) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                    ) {
                        Text(
                            text = trail.name,
                            fontSize = 16.sp,
                            modifier = Modifier.padding(4.dp)
                        )
                        Spacer(modifier = Modifier.weight(1.5f))
                        Image(
                            painter = painterResource(id = trail.imageID),
                            contentDescription = null,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(4.dp)
                                .height(110.dp)
                                .aspectRatio(1f)

                                .clip(shape = RoundedCornerShape(4.dp))
                        )
                    }
                }
            }
        }
    )
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun DetailScreen(title: String, description: String, imageID: Int, stopwatch: Stopwatch, colorScheme: MutableState<Boolean>) {
    val context = LocalContext.current

    val scaffoldState = rememberScaffoldState()
    val s = rememberCoroutineScope()
    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            if (LocalConfiguration.current.orientation != Configuration.ORIENTATION_LANDSCAPE) {
                AppBar(onNavigationItemClick = {
                    s.launch {
                        scaffoldState.drawerState.open()
                    }
                })
            }
        },
        drawerContent = {
                DrawerHeader()
                DrawerBody(items = listOf(
                    MenuItem(
                        id = "1",
                        title = "Zmień motyw",
                        icon = "🎨"
                    ),
                ), onItemClick = {
                    colorScheme.value = !colorScheme.value
                })
        }, drawerGesturesEnabled = scaffoldState.drawerState.isOpen,
        floatingActionButtonPosition = FabPosition.End, floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    Toast.makeText(context, "Otwieram gepardzi aparat", Toast.LENGTH_SHORT).show()
                }) {
                Text("📸", fontSize = 40.sp)
            }},
        content = {
            Details(title, description, imageID, stopwatch)
        })
}

@Composable
fun Details(
    title: String,
    description: String,
    imageID: Int,
    stopwatch: Stopwatch
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top,
    ) {
        Text(
            text = title,
            fontSize = 32.sp,
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Unspecified)
                .padding(8.dp)
        )
        Image(
            painter = painterResource(id = imageID),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
        )
        Text(
            text = description,
            fontSize = 20.sp,
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Unspecified)
                .padding(8.dp)
        )

        Stopwatch(
            timeF = stopwatch.timeF,
            onStart = { stopwatch.start() },
            onPause = { stopwatch.pause() },
            onReset = { stopwatch.reset() })

        Spacer(modifier = Modifier.height(16.dp))
        AnimatedButton()
    }
}

@Composable
fun Stopwatch(
    timeF: String,
    onStart: () -> Unit,
    onPause: () -> Unit,
    onReset: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Text(
            text = timeF,
            fontSize = 32.sp,
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Unspecified)
                .padding(8.dp)
        )
        Spacer(Modifier.height(16.dp))
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxWidth()
        ) {
            Button(onClick = onStart) {
                Text("Start")
            }
            Spacer(Modifier.width(16.dp))
            Button(onClick = onPause) {
                Text("Pause")
            }
            Spacer(Modifier.width(16.dp))
            Button(onClick = onReset) {
                Text("Reset")
            }
        }
    }
}

@Composable
fun DrawerHeader() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.primary)
            .padding(vertical = 16.dp),
        contentAlignment = Alignment.Center,
    ) {
        Column() {
            Text(text = "Gepard", fontSize = 80.sp, color = MaterialTheme.colors.onPrimary)
            Text(text = "🐆", fontSize = 100.sp, modifier = Modifier.align(Alignment.CenterHorizontally))
        }
    }
}

@Composable
fun DrawerBody(
    items: List<MenuItem>,
    modifier: Modifier = Modifier,
    itemTextStyle: TextStyle = TextStyle(fontSize = 24.sp),
    onItemClick: (MenuItem) -> Unit
) {
    LazyColumn(modifier) {
        items(items) { item ->
            Row(modifier = Modifier
                .fillMaxWidth()
                .clickable { onItemClick(item) }
                .padding(16.dp)
            ) {
                Text(text = item.icon, style = itemTextStyle.copy(fontSize = 24.sp))
                Text(text = item.title, style = itemTextStyle.copy(fontSize = 20.sp))
            }
        }
    }
}

@Composable
fun AppBar(
    onNavigationItemClick: () -> Unit
) {
    TopAppBar(
        title = { Text("Gepard Wędrownik 2.0") },
        navigationIcon = {
            IconButton(onClick = { onNavigationItemClick() }) {
                Icon(imageVector = Icons.Filled.Menu, contentDescription = "Toggle Drawer")
            }
        },
        backgroundColor = MaterialTheme.colors.primary,
        contentColor = MaterialTheme.colors.onPrimary
    )
}


@Composable
fun AnimatedButton() {
    val view = LocalView.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        Box(
            modifier = Modifier
                .width(160.dp)
                .height(46.dp)
                .clip(RoundedCornerShape(8.dp))
                .background(gepardColorLight)
                .clickable {
                    animateColorChange(view) {
                    }
                }
        ) {
            Text(
                text = "Świętuj ukończenie",
                fontSize = 16.sp,
                color = Color.White,
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }
}

fun animateColorChange(view: View, onAnimationEnd: (Color) -> Unit) {


    val rotationAnimator = ObjectAnimator.ofFloat(view, "rotation", 0f, 3600f)
    val scaleXAnimator = ObjectAnimator.ofFloat(view, "scaleX", 1f, 1.5f, 1f)
    val scaleYAnimator = ObjectAnimator.ofFloat(view, "scaleY", 1f, 1.5f, 1f)
    val translationXAnimator = ObjectAnimator.ofFloat(view, "translationX", 0f, 200f, 0f)
    val translationYAnimator = ObjectAnimator.ofFloat(view, "translationY", 0f, 200f, 0f)

    val animatorSet = AnimatorSet()
    animatorSet.playTogether(
        rotationAnimator,
        scaleXAnimator,
        scaleYAnimator,
        translationXAnimator,
        translationYAnimator
    )
    animatorSet.duration = 10000
    animatorSet.doOnEnd {
        onAnimationEnd(Color.Blue)
    }
    animatorSet.start()
}